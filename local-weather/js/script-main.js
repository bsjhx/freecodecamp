$(document).ready(function() {
    $('#scaleDiv').click(function() {
        $("#scale").text('F');
    });
});

function start() {
    $().ready(function() {
        generateWeatherSection();
    });
}

function generateWeatherSection() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(createSection);
    } else { 
        return "Geolocation is not supported by this browser.";
    }
}

function createSection(position) {
    var weatherAPILink =  createWeatherAPILink(position.coords.latitude, position.coords.longitude);
    getResponse(weatherAPILink, createCode);
}

function createWeatherAPILink(latitude, longitude) {
    return "https://fcc-weather-api.glitch.me/api/current?lat=" 
        + latitude 
        + "&lon=" 
        + longitude;
}

function getResponse(url, callback) {
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        success: function( data ) {
            callback(data);
        },
        error: function( data ) {
            callback('api_connection_problem');
      }
    });
}

function createCode(json) {
    if (json === 'api_connection_problem') {
        $("#weather").html('<div id="errorDiv" class="col-md-12"><h3>Problem with connection to weather!</h3></div>');
        $("#errorDiv").css("color", "#DB1915");
        return;
    }
    
    var mainWeather = json.weather[0].main;
    var temperature = Math.round(json.main.temp);
    var place = json.name;
    var countryCode = json.sys.country;

    var addressDiv = '<div class="col-md-12"><h3>' + place + ', ' + countryCode + '</h3></div>';

    var weatherDiv = '<div class="col-md-12"><h3><span id="temperature">' 
        + temperature 
        + '</span>&#176;<span id="scale" style="color: #DB1915;">C</span>, ' 
        + mainWeather 
        + '</h3></div>';

    $("#weather").html(addressDiv);
    $("#weather").append(weatherDiv);

    addChangingScaleAfterClick();

    var iconSection = generateIconSection(mainWeather);
    $("#icon").append(iconSection);
}

function addChangingScaleAfterClick() {
    $('#scale').on('click', function() {
        var currentTemp = parseFloat($("#temperature").text());

        if ($("#scale").text() === 'C') {
            $("#scale").text('F');
            $("#temperature").text(Math.round(32 + (9/5) * currentTemp));
        } else {
            $("#scale").text('C');
            $("#temperature").text(Math.round((5/9) * (currentTemp - 32)));
        }
    });
}

function generateIconSection(weather) {
    var result = '<i class="wi ICON_NAME"></i>';
    weather = weather.toLowerCase();
    var icons = prepareIconsNameMap(getTimeOfDay(), weather), iconName = "unknown";

    var key = getTimeOfDay() + '-' + weather;
    if (!(key in icons)) {
        key = "unknown";
    }

    iconName = icons[key];
    result = result.replace('ICON_NAME', iconName);

    return result;
}

function getTimeOfDay() {
    var hour = new Date().getHours();
    if (hour >= 5 && hour <= 20) {
        return "day";
    } else {
        return "night";
    }
}

function prepareIconsNameMap(timeOfDay, weather) {
    var map = {
        "day-drizzle" : "wi-day-rain-mix",
        "night-drizzle" : "wi-night-alt-rain-mix",
        "day-clouds" : "wi-day-cloudy",
        "night-clouds" : "wi-night-alt-cloudy",
        "day-rain" : "wi-day-rain",
        "night-rain" : "wi-night-alt-rain",
        "day-snow" : "wi-day-snow",
        "night-snow" : "wi-night-alt-snow",
        "day-clear" : "wi-day-sunny",
        "night-clear" : "wi-night-clear",
        "day-thunderstorm" : "wi-day-thunderstorm",
        "night-thunderstorm" : "wi-night-alt-thunderstorm",
        "day-mist" : "wi-fog",
        "night-mist" : "wi-fog",
        "day-fog" : "wi-day-fog",
        "night-fog" : "wi-night-fog",
        "unknown" : "wi-alien"
    };

    return map;
}