$(document).ready(function() {
	$("#getMessage").on("click", function() {
		createQuote();
	});
});

function createQuote() {
	$.ajax({
		url: 'https://api.myjson.com/bins/am603',
	    dataType: 'json',
	    success: function( json ) {
			var quoteId = Math.floor(Math.random() * (json.length));
			var currentQuote = json[quoteId]["quote"];
			var currentAuthor = json[quoteId]["author"];

			$(".quote").html(generateQuoteDiv(currentAuthor, currentQuote));

			$('#tweet-quote').attr('href', 'https://twitter.com/intent/tweet?hashtags=quotes&related=freecodecamp&text=' 
				+ encodeURIComponent('"' + currentQuote + '" ~' + currentAuthor));
	    },
	    error: function( data ) {
	      $(".quote").html(generateProblemWithJsonAPIDiv());
	    }
	});
}

function generateQuoteDiv(authorName, quote) {
	return "<h2 id=>" + quote + "</h2></br><p>" + authorName + "</p>";
}

function generateProblemWithJsonAPIDiv() {
	return '<h2 id="errorDiv">Sorry, but there are problems with quotes server. Please try again later :(';
}

function createQuoteOnStart() {
	$(document).ready(function() {
		createQuote();
	});
}

function tweet() {
	$('#tweet-quote').on('click', function() {
    if(!inIframe()) {
      openURL('https://twitter.com/intent/tweet?hashtags=quotes&related=freecodecamp&text=' 
      	+ encodeURIComponent('"' + currentQuote + '" ' + currentAuthor));
    }
  });
}