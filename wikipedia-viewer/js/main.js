function openRandomWikiPage() {
    window.open('https://en.wikipedia.org/wiki/Special:Random', '_blank');
}

function start() {
    var input = document.getElementById("displayWikiArticlesInput");
    input.addEventListener("keyup", function (event) {
        event.preventDefault();
        if (event.keyCode === 13) {
            findArticles(input.value);
        }
    });
};

function findArticlesButton() {
    var input = document.getElementById("displayWikiArticlesInput");
    
    callWikipediaAPI(input.value);
}

function findArticles(searchedText) {
    callWikipediaAPI(searchedText, generateCode);
}

function callWikipediaAPI(searchedText) {
    let url = 'https://en.wikipedia.org/w/api.php?action=opensearch&format=json&smaxage=0&search=' + searchedText;

    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open('GET', url, true);

    xmlHttp.addEventListener('load', function () {
        if (this.status === 200) {
            cleanArticlesDiv();
            generateCode(this.responseText);
        }
    });

    xmlHttp.open("GET", url, true);
    xmlHttp.setRequestHeader('Access-Control-Allow-Origin', 'https://en.wikipedia.org');
    xmlHttp.send();
}

function generateCode(wikiResponse) {
    var responseObj = JSON.parse(wikiResponse);

    for (let i = 0; i < responseObj[1].length; i++) {
        $("#articles").append(createArticleDiv(responseObj[3][i], responseObj[1][i]));
    }
}

function createArticleDiv(url, title) {
    return "<div class='jumbotron'><p><a href=\"" + url + "\" target=\"_blank\">" + title + "</a></p></div>";
}

function cleanArticlesDiv() {
    $("#articles").empty();
}